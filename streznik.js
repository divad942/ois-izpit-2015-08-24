var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});


app.get('/api/seznam', function(req, res) {
	res.send(noviceSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje novic)
 */
app.get('/api/dodaj', function(req, res) {
  // ...
  
  ///api/dodaj?naslov=ois+izpit&povzetek=resevanje+pios+izpita&kategorija=zabava&postnaStevilka=8000&kraj=Novo+mesto&povezava=www.blabla.com
  
  var naslov = req.query.naslov;
  var povzetek = req.query.povzetek;
  var kategorija = req.query.kategorija;
  var postnaStevilka = req.query.postnaStevilka;
  var kraj = req.query.kraj;
  var povezava = req.query.povezava;
  
  if( naslov == undefined || povzetek == undefined || kategorija == undefined || postnaStevilka== undefined || kraj == undefined || povezava == undefined){
    res.send('Napaka pri dodajanju novice!');
  }else{
    // pridobivanje novega id-ja
    var newID;
    var oldID = 0;
    for(var i in noviceSpomin){
      if(noviceSpomin[i].id > oldID){
        oldID = noviceSpomin[i].id;
      }
    }
    newID = oldID + 1;
   
    // shranjevanje nove novice   
    noviceSpomin.push({ id: newID, naslov: naslov, povzetek: povzetek, kategorija: kategorija, postnaStevilka: postnaStevilka, kraj: kraj, povezava: povezava });
    
    res.redirect('/');
  }
  
});



/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje novice)
 */
app.get('/api/brisi', function(req, res) {
  // ...
  
  var idNovice = req.query.id;
  
	var uspesno = false;
	if (idNovice != undefined) {
		for (var i in noviceSpomin) {
			if (noviceSpomin[i].id == idNovice) {
				noviceSpomin.splice(i, 1);
				uspesno = true;
				break;
			}
		}
		if (uspesno) {
			res.redirect('/');
		} else {
			//res.send('Novica z id-jem ' + idNovice * ' ne obstaja.<br/> <a href="javascript:window.history.back()">Nazaj</a>"');
		  res.send('Novica z id-jem ' + idNovice + ' ne obstaja.' + '<br/>' + '<a href="javascript:window.history.back()">Nazaj</a>');
		}
	} else {
		res.send('Napačna zahteva!');
	}
  
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var noviceSpomin = [
  {
    id: 1,
    naslov: 'Slovenija in korupcija: končali smo v družbi Mehike in Kolumbije',
    povzetek: 'Slovenija krši mednarodne zaveze v boju proti podkupovanju, opozarjajo pri slovenski podružnici TI. Konvencijo o boju proti podkupovanju tujih javnih uslužbencev v mednarodnem poslovanju OECD namreč izvajamo "malo ali nič".',
    kategorija: 'novice',
    postnaStevilka: 1000,
    kraj: 'Ljubljana',
    povezava: 'http://www.24ur.com/novice/slovenija/slovenija-in-korupcija-koncali-smo-v-druzbi-mehike-in-kolumbije.html'
  }, {
    id: 2,
    naslov: 'V Postojni udaren začetek festivala z ognjenim srcem',
    povzetek: 'V Postojni se je z nastopom glasbenega kolektiva The Stroj začel tradicionalni dvotedenski festival Zmaj ma mlade.',
    kategorija: 'zabava',
    postnaStevilka: 6230,
    kraj: 'Postojna',
    povezava: 'http://www.rtvslo.si/zabava/druzabna-kronika/v-postojni-udaren-zacetek-festivala-z-ognjenim-srcem/372125'
  }
];
